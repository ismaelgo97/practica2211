package com.curso.java.lambda;

import java.util.function.Function;
import java.util.function.Supplier;

public class LambdaApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		randomNumber();
		toUpperCase("Convertir a mayusculas");
	}
	
	public static void randomNumber() {
		Supplier <Double> supply = Math::random;
		System.out.println(supply.get());
	}
	
	public static void toUpperCase(String texto) {
		Function<String, String> lambdaFunction = text -> text.toUpperCase();
		
		String upperText = lambdaFunction.apply(texto);
		System.out.println(upperText);
	}

}
