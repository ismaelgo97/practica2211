package com.curso.java.stream;

import java.util.List;
import java.util.stream.Stream;

import com.curso.java.optional.optionalPerson;
import com.curso.java.optional.personList;

public class streamApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<optionalPerson> people = new personList().getPeople();
		forEach(people);
		matchIsmael(people);
	}

	public static void forEach(List<optionalPerson> people) {
		Stream<optionalPerson> stream = people.stream();
		stream.forEach(person -> person.getNombre().ifPresent(nombre -> System.out.println("Nombre de la persona: "+nombre)));
	}
	
	public static void matchIsmael(List<optionalPerson> people) {
		Stream<optionalPerson> stream = people.stream();
		System.out.println(stream.anyMatch(person -> person.getNombre().get() == "Ismael"));
	}
}
