package com.curso.java.date;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

public class dateApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LocalDate date = LocalDate.now();
		LocalTime time = LocalTime.now();
		
		System.out.println("Hoy es el d�a " + date.getDayOfMonth() + " de " + date.getMonth() + " de " + date.getYear()+". Lo que corresponde al d�a "+date.getDayOfYear()+" de a�o.");
		
		System.out.println("AHora mismo son las "+time.getHour()+":"+time.getMinute()+":"+time.getSecond());
	}

}
