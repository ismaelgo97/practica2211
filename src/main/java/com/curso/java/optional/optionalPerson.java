package com.curso.java.optional;

import java.util.Optional;

public class optionalPerson {
	private Optional<String> nombre;
	private Optional<Integer> edad;
	
	public optionalPerson(String nombre, Integer edad) {
		this.nombre = Optional.of(nombre);
		this.edad = Optional.of(edad);
	}
	
	public optionalPerson(String nombre) {
		this.nombre = Optional.of(nombre);
		this.edad = Optional.empty();
	}
	
	public optionalPerson(Integer edad) {
		this.nombre = Optional.empty();
		this.edad = Optional.of(edad);
	}

	public Optional<String> getNombre() {
		return nombre;
	}

	public void setNombre(Optional<String> nombre) {
		this.nombre = nombre;
	}

	public Optional<Integer> getEdad() {
		return edad;
	}

	public void setEdad(Optional<Integer> edad) {
		this.edad = edad;
	}
	
	
}
